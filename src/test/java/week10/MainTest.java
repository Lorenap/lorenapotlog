package week10;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void filterByMonth_success() {
        String line = "Piotr,Smitzer,1995.07.07";
        assertTrue(Main.filterByMonth(line,"07"));
    }

    @Test
    void findMonth_success() {
        String date = "2007.02.26";
        assertEquals("02",Main.findMonth.apply(date));
    }

    @Test
    void processStream_success() {

        Stream<String> stream = Stream.of("Michael,Gonsil,1989.05.12","Alizee,Baron,1991.05.17","Umar,Jorgson,2004.03.21");
        List<String> list = Main.processStream(stream,"05");

        assertEquals("Alizee,Baron,1991.05.17",list.get(0));
        assertEquals("Michael,Gonsil,1989.05.12",list.get(1));
        assertEquals(2,list.size());
    }
}