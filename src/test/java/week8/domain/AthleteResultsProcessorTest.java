package week8.domain;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AthleteResultsProcessorTest {

    @Test
    void convertToShootingResults_success() {

        List<ShootingResult> shootingResults = AthleteResultsProcessor.convertToShootingResults("xxxox");
        assertEquals(5,shootingResults.size());
        assertEquals(ShootingResult.HIT, shootingResults.get(0));
        assertEquals(ShootingResult.HIT, shootingResults.get(1));
        assertEquals(ShootingResult.HIT, shootingResults.get(2));
        assertEquals(ShootingResult.MISS, shootingResults.get(3));
        assertEquals(ShootingResult.HIT, shootingResults.get(4));
    }

    @Test
    void processLine_success() {
        String[] line = {"15","Brad Baker","AT","45:00","xooxx","xxxox","xxoxx"};
        Athlete athlete = AthleteResultsProcessor.processLine(line);

        assertEquals("15", athlete.getAthleteNumber());
        assertEquals("Brad Baker", athlete.getAthleteName());
        assertEquals("AT", athlete.getCountryCode());
        assertEquals("PT45M", athlete.getSkiTimeResult().toString());
        assertEquals("xooxx", athlete.getFirstShootingRange());
        assertEquals("xxxox", athlete.getSecondShootingRange());
        assertEquals("xxoxx", athlete.getThirdShootingRange());

        assertEquals("PT45M40S", athlete.getFinalTime().toString());

    }

    @Test
    void getPenaltySeconds_success() {
        int penaltySeconds = AthleteResultsProcessor.getPenaltySeconds("xxxox");
        assertEquals(10,penaltySeconds);
    }
}