package week6.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static week6.domain.UnitType.CM;

class ConvertorTest {

    Convertor convertor;
    private MetricValue value;
    private MetricValue result;

    @BeforeEach
    void setUp() {
        convertor = new Convertor();
        value = new MetricValue(24, UnitType.M);
        result = new MetricValue(6500, UnitType.MM);
    }

    @ParameterizedTest
    @MethodSource("generateData")
    void setUnitTypeToMM_success(MetricValue value) {
        convertor.setUnitTypeToMM(value);
        assertSame(UnitType.MM, value.getUnitType());
    }

    private static Stream<Arguments> generateData() {
        return Stream.of(
                Arguments.of(new MetricValue(100, CM)),
                Arguments.of(new MetricValue(200, UnitType.DM)),
                Arguments.of(new MetricValue(1, UnitType.M))
        );
    }

    @Test
    void setUnitTypeToMM_distanceConversion_success() {
        convertor.setUnitTypeToMM(value);
        assertEquals(24000, value.getDistance());
        assertEquals(UnitType.MM, value.getUnitType());
    }

    @Test
    void getOutputUnitType_success() {
        convertor.getOutputUnitType(result, CM);
        assertEquals(CM, result.getUnitType());
        assertEquals(650, result.getDistance());
    }

    @Test
    @DisplayName("Checks if getOutputUnitType throws exception when input type is different than MM")
    void getOutputUnitType_inputUnitTypeDifferentThanMM_exception() {
        Exception exception = assertThrows(RuntimeException.class, () -> convertor.getOutputUnitType(value, CM));

        //Checks if exception message is displayed correctly

        assertEquals("Please insert a MM value", exception.getMessage());
    }
}