package week6.domain;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@Suite
@SuiteDisplayName("Lorena's tests")
@SelectClasses({CalculatorTest.class, ConvertorTest.class})
public class TestSuite {
}
