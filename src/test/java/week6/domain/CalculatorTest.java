package week6.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static week6.domain.UnitType.MM;

class CalculatorTest {

    Calculator calculator = new Calculator();
    private MetricValue value1;
    private MetricValue value2;

    @BeforeEach
    void setUp() {
        value1 = new MetricValue(100, UnitType.MM);
        value2 = new MetricValue(23, UnitType.CM);
    }

    @Test
    void addDistance_success() {
        assertEquals(new MetricValue(330, MM), calculator.addDistance(value1, value2));
        assertEquals(UnitType.MM, value1.getUnitType(), "Unit Type for value 1 should be MM");
        assertEquals(UnitType.MM, value2.getUnitType(), "Unit Type for value 2 should be MM");
    }

    @Test
    void subtractDistance_success() {
        assertEquals(new MetricValue(130, MM), calculator.subtractDistance(value2, value1));
        assertEquals(UnitType.MM, value1.getUnitType());
        assertEquals(UnitType.MM, value2.getUnitType());
    }

    @Test
    @DisplayName("Check if exception is thrown for second distance bigger")
    void subtractDistance_secondValueBigger_exception() {
        Exception exception = assertThrows(RuntimeException.class, () -> calculator.subtractDistance(value1, value2));

        //Checks if exception message is displayed correctly

        assertEquals("Cannot subtract. Distance 2 > Distance 1.", exception.getMessage());
    }
}