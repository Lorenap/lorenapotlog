package week9.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StudentRepositoryTest {

    StudentRepository studentRepository;
    Student student;

    @BeforeEach
    public void init() {
        studentRepository = new StudentRepository();
        student = new Student("Matei", "Popa", Gender.convert("m"), LocalDate.of(1989, 2, 24), "18739203827314");
    }

    @Test
    void add_success() {
        studentRepository.add(student);
        assertEquals(1, studentRepository.students.size());
    }

    @Test
    void deleteByCNP_CNPisNull_exception() {
        Exception e = assertThrows(LorenaValidationException.class, () -> studentRepository.deleteByCNP(null));
        assertEquals("CNP is empty. Please add valid CNP.", e.getMessage());
    }

    @Test
    void deleteByCNP_studentNotFound_Exception() {
        studentRepository.add(student);
        Exception e = assertThrows(LorenaValidationException.class, () -> studentRepository.deleteByCNP("18739453827314"));
        assertEquals("Student does not exist.", e.getMessage());
    }

    @Test
    void deleteByCNP_success() {
        studentRepository.add(student);
        studentRepository.deleteByCNP("18739203827314");
        assertEquals(0, studentRepository.students.size());
    }

    @Test
    void retrieveStudents_ageNotANumber_exception() {
        Exception e = assertThrows(LorenaValidationException.class, () -> studentRepository.retrieveStudents("a1"));
        assertEquals("Age is not a number.", e.getMessage());
    }

    @Test
    void retrieveStudents_ageIsNegative_exception() {
        Exception e = assertThrows(LorenaValidationException.class, () -> studentRepository.retrieveStudents("-2"));
        assertEquals("Age is negative.", e.getMessage());
    }

    @Test
    void retrieveStudents_success() {
        studentRepository.add(student);
        assertEquals(1, studentRepository.retrieveStudents("33").size());
    }
}