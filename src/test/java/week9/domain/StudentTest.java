package week9.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StudentTest {

    @Test
    public void createStudent_emptyFirstName_exception() {
        Exception e = assertThrows(LorenaValidationException.class, () ->
                new Student(null, "A", Gender.FEMALE, LocalDate.now(), "125"));
        assertEquals("First name and last name should not be empty. Please fill in both fields", e.getMessage());
    }

    @Test
    public void createStudent_youngerThan18_exception() {
        Exception e = assertThrows(LorenaValidationException.class, () ->
                new Student("B", "A", Gender.FEMALE, LocalDate.of(2005, 1, 1), "125"));
        assertEquals("All students must be 18 or older", e.getMessage());
    }

    @Test
    public void createStudent_bornBefore1900_exception() {
        Exception e = assertThrows(LorenaValidationException.class, () ->
                new Student("B", "A", Gender.FEMALE, LocalDate.of(1890, 1, 1), "125"));
        assertEquals("Birthdate must be 1900 or above.", e.getMessage());
    }

    @Test
    public void createStudent_nullCNP_exception() {
        Exception e = assertThrows(LorenaValidationException.class, () ->
                new Student("B", "A", Gender.FEMALE, LocalDate.of(2001, 1, 1), null));
        assertEquals("Wrong CNP. Please try again", e.getMessage());
    }

    @Test
    public void createStudent_wrongCNP_exception() {
        Exception e = assertThrows(LorenaValidationException.class, () ->
                new Student("B", "A", Gender.FEMALE, LocalDate.of(2001, 1, 1), "432"));
        assertEquals("Wrong CNP. Please try again", e.getMessage());
    }

}