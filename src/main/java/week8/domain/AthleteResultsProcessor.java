package week8.domain;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class AthleteResultsProcessor {

    /**
     * @param results shooting range results (eg. xxoxx) as a String
     * @return list of enums (eg. [HIT, HIT, MISS, HIT, HIT])
     */
    public static List<ShootingResult> convertToShootingResults(String results) {
        List<ShootingResult> shootingResults = new ArrayList<>();
        for (int i = 0; i < results.length(); i++) {
            char c = results.charAt(i);
            shootingResults.add(ShootingResult.convert(c));
        }
        return shootingResults;
    }

    /**
     * @param csvName Name of CSV file
     * @return Reads values from CSV file, builds Athlete objects and adds them to a sorted by final time list;
     * @throws IOException
     */
    public static List<Athlete> getListfromCSVfile (String csvName) throws IOException {
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(
                    ClassLoader.getSystemResource(csvName).toURI()));
        } catch (URISyntaxException | IOException e) {
            System.out.println("Encountered an error: " + e.getMessage());
        }

        if (reader == null) {
            throw new RuntimeException("Reader not initialized");
        }

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(',')
                .build();

        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withCSVParser(parser)
                .build();

        String[] line;
        List<Athlete> athletes = new ArrayList<>();

        while ((line = csvReader.readNext()) != null) {
            Athlete athlete = processLine(line);
            athletes.add(athlete);
        }

        athletes.sort(new AthleteComparator()); //Sort athletes by final time
        return athletes;
    }

    public static Athlete processLine(String[] line) {
        String[] skiTimeResultParts = line[3].split(":");
        //Reades String from CSV file (eg. "30:21") and converts it to Duration (eg. 30M21S)
        Duration skiTimeResult = Duration.ofMinutes(Long.parseLong(skiTimeResultParts[0])).plusSeconds(Long.parseLong(skiTimeResultParts[1]));

        Athlete athlete = new Athlete(line[0], line[1], line[2], skiTimeResult, line[4], line[5], line[6]);
        athlete.setFinalTime();
        return athlete;
    }

    /**
     * @param shootingResult shooting range results (eg. xxoxx) as a String
     * @return Total of penalties for all 3 shooting ranges in seconds
     */
    public static int getPenaltySeconds(String shootingResult) {
        List<ShootingResult> list1 = AthleteResultsProcessor.convertToShootingResults(shootingResult);

        int penaltySeconds = 0;
        for (ShootingResult a : list1) {
            if (a == ShootingResult.MISS) {
                penaltySeconds += 10;
            }
        }
        return penaltySeconds;
    }
}
