package week8.domain;

import java.util.Comparator;

public class AthleteComparator implements Comparator<Athlete> {

    @Override
    public int compare(Athlete o1, Athlete o2) {
        return o1.getFinalTime().compareTo(o2.getFinalTime());
    }
}
