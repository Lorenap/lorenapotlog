package week8.domain;

public enum ShootingResult {
    HIT("x"), MISS("o");

    private final String shot;

    ShootingResult(String shot) {
        this.shot = shot;
    }

    /**
     * @param c 'x' or 'o'
     * @return  enum constant HIT or MISS
     */
    public static ShootingResult convert(Character c) {
        for (ShootingResult a : ShootingResult.values()) {
            if (a.getShot().equals(String.valueOf(c))) {
                return a;
            }
        }
        return null;
    }

    public String getShot() {
        return shot;
    }
}
