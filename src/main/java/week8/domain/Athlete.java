package week8.domain;

import java.time.Duration;
import java.util.List;

public class Athlete {

    private final String athleteNumber;
    private final String athleteName;
    private final String countryCode;
    private final Duration skiTimeResult;
    private final String firstShootingRange;
    private final String secondShootingRange;
    private final String thirdShootingRange;

    private Duration finalTime;
    private int penaltySeconds = 0;

    public Athlete(String athleteNumber, String athleteName, String countryCode, Duration skiTimeResult, String firstShootingRange, String secondShootingRange, String thirdShootingRange) {
        this.athleteNumber = athleteNumber;
        this.athleteName = athleteName;
        this.countryCode = countryCode;
        this.skiTimeResult = skiTimeResult;
        this.firstShootingRange = firstShootingRange;
        this.secondShootingRange = secondShootingRange;
        this.thirdShootingRange = thirdShootingRange;
    }

    public String getAthleteNumber() {
        return athleteNumber;
    }

    public String getAthleteName() {
        return athleteName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public Duration getSkiTimeResult() {
        return skiTimeResult;
    }

    public String getFirstShootingRange() {
        return firstShootingRange;
    }

    public String getSecondShootingRange() {
        return secondShootingRange;
    }

    public String getThirdShootingRange() {
        return thirdShootingRange;
    }

    public Duration getFinalTime() {
        return finalTime;
    }


    public void setFinalTime() {

        this.penaltySeconds += AthleteResultsProcessor.getPenaltySeconds(firstShootingRange);
        this.penaltySeconds += AthleteResultsProcessor.getPenaltySeconds(secondShootingRange);
        this.penaltySeconds += AthleteResultsProcessor.getPenaltySeconds(thirdShootingRange);

        this.finalTime = this.skiTimeResult.plusSeconds(this.penaltySeconds);
    }

    public int getPenaltySeconds() {
        return penaltySeconds;
    }

    @Override
    public String toString() {
        return "Athlete{" +
                "athleteNumber='" + athleteNumber + '\'' +
                ", athleteName='" + athleteName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", skiTimeResult=" + skiTimeResult +
                ", firstShootingRange='" + firstShootingRange + '\'' +
                ", secondShootingRange='" + secondShootingRange + '\'' +
                ", thirdShootingRange='" + thirdShootingRange + '\'' +
                ", finalTime=" + finalTime +
                '}';
    }

}
