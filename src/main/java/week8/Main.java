package week8;

import org.apache.commons.lang3.time.DurationFormatUtils;
import week8.domain.Athlete;
import week8.domain.AthleteResultsProcessor;

import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        List<Athlete> athletes = AthleteResultsProcessor.getListfromCSVfile("results_biathlon.csv");

        System.out.println(athletes);

        System.out.println("RESULTS");
        System.out.println("Winner - " + athletes.get(0).getAthleteName() + " " + DurationFormatUtils.formatDuration(athletes.get(0).getFinalTime().toMillis(), "m':'s") + " (" + DurationFormatUtils.formatDuration(athletes.get(0).getSkiTimeResult().toMillis(), "m':'s") + " + " + athletes.get(0).getPenaltySeconds() + ")");
        System.out.println("Runner-up - " + athletes.get(1).getAthleteName() + " " + DurationFormatUtils.formatDuration(athletes.get(1).getFinalTime().toMillis(), "m':'s") + " (" + DurationFormatUtils.formatDuration(athletes.get(1).getSkiTimeResult().toMillis(), "m':'s") + " + " + athletes.get(1).getPenaltySeconds() + ")");
        System.out.println("Third Place - " + athletes.get(2).getAthleteName() + " " + DurationFormatUtils.formatDuration(athletes.get(2).getFinalTime().toMillis(), "m':'s") + " (" + DurationFormatUtils.formatDuration(athletes.get(2).getSkiTimeResult().toMillis(), "m':'s") + " + " + athletes.get(2).getPenaltySeconds() + ")");

    }
}
