package week9.domain;

public enum Gender {
    MALE("M"), FEMALE("F");

    private String gender;

    Gender(String g) {
        this.gender= g;
    }

    public static Gender convert(String c) {
        for (Gender g : Gender.values()) {
            if (g.getGender().equalsIgnoreCase(c)) {
                return g;
            }
        }
        return null;
    }

    public String getGender() {
        return gender;
    }
}



