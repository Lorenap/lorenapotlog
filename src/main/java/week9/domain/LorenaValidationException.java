package week9.domain;

public class LorenaValidationException extends RuntimeException{

    public LorenaValidationException(String message) {
        super(message);
    }
}
