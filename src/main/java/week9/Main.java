package week9;

import week9.domain.Gender;
import week9.domain.Student;
import week9.domain.StudentRepository;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {

        StudentRepository studentRepository = new StudentRepository();

        Student student1 = new Student("Matei", "Popa", Gender.convert("m"), LocalDate.of(1989, 2, 24), "18739203827314");
        Student student2 = new Student("Maria", "Ionescu", Gender.convert("f"), LocalDate.of(2001, 4, 21), "22739205127314");
        Student student3 = new Student("Alexandru", "Mihailescu", Gender.convert("M"), LocalDate.of(1989, 2, 3), "19739203827314");
        Student student4 = new Student("Dragos", "Greba", Gender.MALE, LocalDate.of(1998, 2, 12), "19739203827314");
        Student student5 = new Student("Marius", "Ciubatoru", Gender.convert("m"), LocalDate.of(1950, 5, 7), "10735221727314");
        Student student6 = new Student("Andreea", "Popa", Gender.FEMALE, LocalDate.of(2003, 7, 19), "22735423697314");
        Student student7 = new Student("Ioana", "Chiriac", Gender.convert("F"), LocalDate.of(1974, 12, 4), "28733926987314");

        studentRepository.add(student1);
        studentRepository.add(student2);
        studentRepository.add(student3);
        studentRepository.add(student4);
        studentRepository.add(student5);
        studentRepository.add(student6);
        studentRepository.add(student7);

        studentRepository.listStudents();

        studentRepository.deleteByCNP("22735423697314"); //delete Andreea;
        studentRepository.listStudents();

        System.out.println(studentRepository.retrieveStudents("33"));
    }
}

