package week11;

import week11.domain.FestivalAttendeeThread;
import week11.domain.FestivalGate;
import week11.domain.FestivalStatisticsThread;
import week11.domain.TicketType;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {

    private static final List<TicketType> VALUES = Collections.unmodifiableList(Arrays.asList(TicketType.values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    // Return a random ticket type
    public static TicketType randomTicket() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }

    public static void main(String[] args) {

        FestivalGate festivalGate = new FestivalGate();
        FestivalStatisticsThread statisticsThread = new FestivalStatisticsThread(festivalGate);
        statisticsThread.start();

        for (int i = 0; i < 165; i++) {
            FestivalAttendeeThread attendeeThread = new FestivalAttendeeThread(randomTicket(), festivalGate);
            attendeeThread.start();
        }
    }
}
