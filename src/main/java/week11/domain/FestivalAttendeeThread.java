package week11.domain;

import java.util.Random;

public class FestivalAttendeeThread extends Thread {

    private final TicketType ticketType;
    private final FestivalGate gate;

    public FestivalAttendeeThread(TicketType ticketType, FestivalGate gate) {
        this.ticketType = ticketType;
        this.gate = gate;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(new Random().nextInt(1000, 8000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        gate.validateTicket(ticketType);
        System.out.println("Thread with ID " + this.getId() + " started");
    }
}
