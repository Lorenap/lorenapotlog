package week11.domain;

import java.util.List;

public class FestivalStatisticsThread extends Thread {

    private final FestivalGate gate;

    public FestivalStatisticsThread(FestivalGate gate) {
        this.gate = gate;
    }

    private int countTickets(TicketType ticketType) {
        List<TicketType> list = gate.getQueue().stream().filter(elem -> elem == ticketType).toList();
        return list.size();
    }

    @Override
    public void run() {
        for (; ; ) {
            try {
                Thread.sleep(5 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!gate.getQueue().isEmpty()) {
                System.out.println();
                System.out.println("STATISTICS");
                System.out.println(gate.getQueue().size() + " people entered");
                System.out.println(countTickets(TicketType.FULL) + " people have full tickets");
                System.out.println(countTickets(TicketType.FREE_PASS) + " people have free passes");
                System.out.println(countTickets(TicketType.FULL_VIP) + " people have full VIP passes");
                System.out.println(countTickets(TicketType.ONE_DAY) + " people have one-day passes");
                System.out.println(countTickets(TicketType.ONE_DAY_VIP) + " people have one-day VIP passes");
                System.out.println();
            }

        }
    }
}
