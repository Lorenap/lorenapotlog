package week5;

import week5.domain.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {

       //Define person

        Person p1 = new Unemployed("Maria", "658412", 20);
        Person p2 = new Student("Alex", "43235", 45);
        Person p3 = new Student("Bogdan", "45231", 67);
        Person p4 = new Unemployed("Cristi", "86695", 32);
        Person p5 = new Hired("Miha", "346342", 20);
        Person p6 = new Hired("Alex", "585343", 45);
        Person p7 = new Student("Mihai", "36342", 14);
        Person p8 = new Hired("Daria", "745343", 18);
        Person p9 = new Unemployed("Maria", "68357", 24);
        Person p10 = new Hired("Bianca", "657235", 67);

       //TREESET

        //TreeSet sorted by Name - alphabetical order;

        Set<Person> setOfPeople = new TreeSet<>(Person.sortByName());
        setOfPeople.add(p1); //same name with p9 & different age;
        setOfPeople.add(p2); //same name & age with p6
        setOfPeople.add(p3);
        setOfPeople.add(p6);
        setOfPeople.add(p7);
        setOfPeople.add(p9);

        System.out.println(setOfPeople);
        //iterate through the TreeSet and print the name and the age;
        for(Person p : setOfPeople){
            System.out.println("Name:" + p.getName() + "\t" + "Age:" + p.getAge() + ";");
        }

        //TreeSet sorted by age - ascending order;

        setOfPeople = new TreeSet<>(Person.sortByAge());
        setOfPeople.add(p1); //same age with p5 & different names
        setOfPeople.add(p2); //same name & age with p6
        setOfPeople.add(p3);
        setOfPeople.add(p5);
        setOfPeople.add(p6);
        setOfPeople.add(p7);
        setOfPeople.add(p9);

        System.out.println(setOfPeople);
        //iterate through the TreeSet and print the name and the age;
        for(Person p : setOfPeople){
            System.out.println("Name:" + p.getName() + "\t" + "Age:" + p.getAge() + ";");
        }

        //TreeSet sorted by names; if names are equal, it compares the age;
        //In comparison to the above TreeSets, it will also return people with same name but different age;

        setOfPeople = new TreeSet<>(Person.sortByName().thenComparing(Person.sortByAge()));
        setOfPeople.add(p1);
        setOfPeople.add(p2);
        setOfPeople.add(p3);
        setOfPeople.add(p4);
        setOfPeople.add(p5);
        setOfPeople.add(p6);
        setOfPeople.add(p7);
        setOfPeople.add(p8);
        setOfPeople.add(p9);
        setOfPeople.add(p10);

        System.out.println(setOfPeople);
        //iterate through the TreeSet and print the name and the age;
        for(Person p : setOfPeople){
            System.out.println("Name:" + p.getName() + "\t" + "Age:" + p.getAge() + ";");
        }

        //HASHMAP

        //Define adresses where hobbies can be practiced;

        Address address1 = new Address("Romania","Pine", 24, 453198);
        Address address2 = new Address("Poland","Oak", 1, 742987);
        Address address3 = new Address("Austria","Elm", 12, 753252);
        Address address4 = new Address("Germany","Ninth", 45, 542145);
        Address address5 = new Address("Romania","Hill", 32, 245846);
        Address address6 = new Address("Bulgaria","Lake", 5, 123578);
        Address address7 = new Address("Russia","Mapple", 12, 223145);
        Address address8 = new Address("Italy","Main", 3, 521343);


        //Define lists of addresses for each hobby and add addresses;

        List<Address> addressesForSwimming = new ArrayList<>();
        addressesForSwimming.add(address1);
        addressesForSwimming.add(address2);
        addressesForSwimming.add(address3);

        List<Address> addressesForGym = new ArrayList<>();
        addressesForGym.add(address4);
        addressesForGym.add(address5);

        List<Address> addressesForCycling = new ArrayList<>();
        addressesForCycling.add(address6);
        addressesForCycling.add(address7);
        addressesForCycling.add(address8);

        List<Address> addressesForFootball = new ArrayList<>();
        addressesForFootball.add(address1);
        addressesForFootball.add(address6);

        //Define hobbies

        Hobby swimming = new Hobby("Swimming", 4,addressesForSwimming);
        Hobby gym = new Hobby("Gym", 5,addressesForGym);
        Hobby cycling = new Hobby("Cycling", 2,addressesForCycling);
        Hobby football = new Hobby("Football", 3,addressesForFootball);

        //Define list of hobbies for adults (over 40 years old)
        List<Hobby> hobbiesOver40 = new ArrayList<>();
        hobbiesOver40.add(swimming);
        hobbiesOver40.add(cycling);

        //Define list of hobbies for youngsters (under 40 years old)
        List<Hobby> hobbiesUnder40 = new ArrayList<>();
        hobbiesUnder40.add(gym);
        hobbiesUnder40.add(football);

        //Define new HashMap

        Map<Person,List<Hobby>> personToHobbies = new HashMap<>();
        personToHobbies.put(p1,hobbiesUnder40);
        personToHobbies.put(p2,hobbiesOver40);
        personToHobbies.put(p3,hobbiesOver40);
        personToHobbies.put(p4,hobbiesUnder40);
        personToHobbies.put(p5,hobbiesUnder40);
        personToHobbies.put(p6,hobbiesOver40);
        personToHobbies.put(p7,hobbiesUnder40);
        personToHobbies.put(p8,hobbiesUnder40);
        personToHobbies.put(p9,hobbiesUnder40);
        personToHobbies.put(p10,hobbiesOver40);

        //Print the names of the hobbies and the countries where it can be practiced

        personToHobbies.get(p2);
        for(Hobby hobby : personToHobbies.get(p2)) {
            System.out.print("Hobby:" + hobby.getHobbyName() + "\nCountries:");
            for(Address address : hobby.getAddresses()) {
                System.out.print(address.getCountry() + ";");
            }
            System.out.println();
        }

        System.out.println("yyy");
    }
}
