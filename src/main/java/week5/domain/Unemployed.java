package week5.domain;

import java.util.List;

public class Unemployed extends Person {


    public Unemployed(String name, String CNP, int age) {
        super(name, CNP, age);
    }

    @Override
    public String toString() {
        return "Unemployed{" +
                " " + super.toString();
    }
}
