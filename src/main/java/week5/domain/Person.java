package week5.domain;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Person {

    private String name;
    private String CNP;

    private int age;

    public Person(String name, String CNP, int age) {
        this.name = name;
        this.CNP = CNP;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static Comparator<Person> sortByName(){
        return new Comparator<Person>() {

            @Override
            public int compare(Person p1, Person p2) {
                return p1.getName().compareTo(p2.getName());
            }
        };
    }

    public static Comparator<Person> sortByAge(){
        return new Comparator<Person>() {

            @Override
            public int compare(Person p1, Person p2) {
                return p1.getAge() - p2.getAge();
            }
        };
    }

    @Override
    public boolean equals(Object person) {
        if (person == null){
            return false;
        }
        if(!(person instanceof Person)){
            return false;
        }
        if(this == person){
            return true;
        } else {
            return this.name.equals(((Person)person).name) && this.age == ((Person)person).age;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(CNP);
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", CNP='" + CNP + '\'' +
                ", age=" + age +
               '}';
    }
}
