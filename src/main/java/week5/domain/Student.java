package week5.domain;

import java.util.List;

public class Student extends Person{


    public Student(String name, String CNP, int age) {
        super(name, CNP, age);
    }

    @Override
    public String toString() {
        return "Student{" +
                " " + super.toString();
    }
}
