package week5.domain;

import java.util.ArrayList;
import java.util.List;

public class Hobby {

    private String hobbyName;
    private int frequency;
    private List<Address> addresses = new ArrayList<>();

    public Hobby(String hobbyName, int frequency, List<Address> adresses) {
        this.hobbyName = hobbyName;
        this.frequency = frequency;
        this.addresses = adresses;
    }

    public String getHobbyName() {
        return hobbyName;
    }

    public void setHobbyName(String hobbyName) {
        this.hobbyName = hobbyName;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> adresses) {
        this.addresses = adresses;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "hobbyName='" + hobbyName + '\'' +
                ", frequency=" + frequency +
                ", adresses=" + addresses +
                '}';
    }
}
