package week5.domain;

import java.util.List;

public class Hired extends Person{


    public Hired(String name, String CNP, int age) {
        super(name, CNP, age);
    }

    @Override
    public String toString() {
        return "Hired{" +
                " " + super.toString();
    }
}
