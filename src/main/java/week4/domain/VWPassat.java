package week4.domain;

import week4.FuelType;

public class VWPassat extends Volkswagen{

    final String chassisNumber;

    public VWPassat(int fuelTankSize, FuelType fuelType, int numberOfGears, float consumptionPer100Km, float availableFuel, int tireSize, String chassisNumber) {
        super(fuelTankSize, fuelType, numberOfGears, consumptionPer100Km, availableFuel, tireSize);
        this.chassisNumber = chassisNumber;
    }

    @Override
    public float getConsumptionFactor(){
        return 0.01f;
    }

    @Override
    public String toString() {
        return "VWPassat{" +
                "chassisNumber='" + chassisNumber + '\'' +
                ", " + super.toString();
    }
}
