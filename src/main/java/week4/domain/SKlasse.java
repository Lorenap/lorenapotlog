package week4.domain;

import week4.FuelType;

public class SKlasse extends Mercedes{

    final String chassisNumber;

    public SKlasse(int fuelTankSize, FuelType fuelType, int numberOfGears, float consumptionPer100Km, float availableFuel, int tireSize, String chassisNumber) {
        super(fuelTankSize, fuelType, numberOfGears, consumptionPer100Km, availableFuel, tireSize);
        this.chassisNumber = chassisNumber;
    }

    @Override
    public float getConsumptionFactor(){
        return 0.05f;
    }

    @Override
    public String toString() {
        return "SKlasse{" +
                "chassisNumber='" + chassisNumber + '\'' +
                ", " + super.toString();
    }
}
