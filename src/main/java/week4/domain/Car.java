package week4.domain;

import week4.FuelType;
import week4.Vehicle;

public abstract class Car implements Vehicle {

    // properties of a Car model which can not change
    private final int fuelTankSize;
    private final FuelType fuelType;
    private final int numberOfGears;
    private final float consumptionPer100Km;

    // properties that are configurable
    private float availableFuel;
    private int tireSize;

    // properties used in logic
    private float currentConsumption = 0;
    private int currentGear = 0;

    public Car(int fuelTankSize, FuelType fuelType, int numberOfGears, float consumptionPer100Km, float availableFuel, int tireSize) {
        this.fuelTankSize = fuelTankSize;
        this.fuelType = fuelType;
        this.numberOfGears = numberOfGears;
        this.consumptionPer100Km = consumptionPer100Km;
        this.availableFuel = availableFuel;
        this.tireSize = tireSize;
    }

    // setters for changing configurable properties
    public void setTireSize(int tireSize) {
        this.tireSize = tireSize;
    }

    public void setAvailableFuel(int availableFuel){
        this.availableFuel = availableFuel;
    }

    @Override
    public void start() {
        this.currentConsumption = 0; // on every start() the consumption stats are reset
        this.currentGear = 0; // every car starts in Neutral gear, in our case is 0
        System.out.println("Your available fuel is: " + availableFuel);
    }

    public void shiftGear(int gear){
        this.currentGear = gear;
    }

    /**
     * @return Some cars might have a decrease in consumption
     * every time they shift up and the same increase when they shift down
     */
    public abstract float getConsumptionFactor();

    @Override
    public void drive(float distance) {
        if(currentGear == 0){
            currentConsumption = 0;
        } else {
            /* currentConsumption is how much fuel was consumed for the given distance;
            * the formula used shows a decrease / increase in consumption (by consumption factor) based on
            * the gear shift */
            currentConsumption = currentConsumption + distance
                    * (this.consumptionPer100Km / 100 - (this.currentGear - 1) * getConsumptionFactor());
            availableFuel = availableFuel - currentConsumption;
            System.out.println(currentConsumption + " " +availableFuel);
        }
    }

    @Override
    public void stop() {
        System.out.println("Your car has stopped.");
        System.out.println("Your current consumption is " + currentConsumption +
                " and your available fuel is: " + availableFuel);
    }

    @Override
    public String toString() {
        return "fuelTankSize=" + fuelTankSize +
                ", fuelType=" + fuelType +
                ", numberOfGears=" + numberOfGears +
                ", consumptionPer100Km=" + consumptionPer100Km +
                ", availableFuel=" + availableFuel +
                ", tireSize=" + tireSize +
                '}';
    }
}
