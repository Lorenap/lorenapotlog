package week4.domain;

import week4.FuelType;

public class VWGolf extends Volkswagen{

    final String chassisNumber;

    public VWGolf(int fuelTankSize, FuelType fuelType, int numberOfGears, float consumptionPer100Km, float availableFuel, int tireSize, String chassisNumber) {
        super(fuelTankSize, fuelType, numberOfGears, consumptionPer100Km, availableFuel, tireSize);
        this.chassisNumber = chassisNumber;
    }

    @Override
    public float getConsumptionFactor(){
        return 0.02f;
    }

    @Override
    public String toString() {
        return "VWGolf{" +
                "chassisNumber='" + chassisNumber + '\'' +
                ", " + super.toString();
    }
}
