package week4.domain;

import week4.FuelType;

public abstract class Mercedes extends Car{

    public Mercedes(int fuelTankSize, FuelType fuelType, int numberOfGears, float consumptionPer100Km, float availableFuel, int tireSize) {
        super(fuelTankSize, fuelType, numberOfGears, consumptionPer100Km, availableFuel, tireSize);
    }

    @Override
    public String toString() {
        return "car brand: Mercedes, " + super.toString();
    }
}
