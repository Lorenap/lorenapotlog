package week4.domain;

import week4.FuelType;

public class CKlasse extends Mercedes{

    final String chassisNumber;

    public CKlasse(int fuelTankSize, FuelType fuelType, int numberOfGears, float consumptionPer100Km, float availableFuel, int tireSize, String chassisNumber) {
        super(fuelTankSize, fuelType, numberOfGears, consumptionPer100Km, availableFuel, tireSize);
        this.chassisNumber = chassisNumber;
    }

    @Override
    public float getConsumptionFactor(){
        return 0.04f;
    }

    @Override
    public String toString() {
        return "CKlasse{" +
                "chassisNumber='" + chassisNumber + '\'' +
                "," + super.toString();
    }
}
