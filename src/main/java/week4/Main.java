package week4;

import week4.domain.CKlasse;
import week4.domain.Car;
import week4.domain.VWGolf;

public class Main {
    public static void main(String[] args) {

        //Create a Volkswagen - VWGolf

        Car golf1 = new VWGolf(120, FuelType.PETROL, 4,4.5f,60, 15,"SV30-0169266");

        System.out.println(golf1);

        golf1.start();
        golf1.shiftGear(1);
        golf1.drive(80);// drives 80 KMs
        golf1.shiftGear(2);
        golf1.drive(1);
        golf1.shiftGear(3);
        golf1.drive(41);
        golf1.shiftGear(4);
        golf1.drive(5);
        golf1.shiftGear(4);
        golf1.drive(2);
        golf1.shiftGear(3);
        golf1.drive(20);
        golf1.shiftGear(4);
        golf1.drive(2);
        golf1.shiftGear(3);
        golf1.drive(1);
        golf1.stop();

        System.out.println(golf1);

        //Create a Mercedes - CKlasse

        Car cKlasse1 = new CKlasse(250, FuelType.DIESEL, 6,7.5f,180, 18,"SV21-0182166");

        System.out.println(cKlasse1);

        cKlasse1.start();
        cKlasse1.shiftGear(1);
        cKlasse1.drive(200);
        cKlasse1.shiftGear(2);
        cKlasse1.drive(50);
        cKlasse1.stop();

        System.out.println(cKlasse1);

    }
}
