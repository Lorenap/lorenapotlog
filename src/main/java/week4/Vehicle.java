package week4;

public interface Vehicle {

    void start();
    void stop();

    /**
     * @param distance is measured in KMs
     */
    void drive(float distance);
}
