package week13;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Main {

    public static void main(String[] args) {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/booking", "postgres", "lorena");

            PreparedStatement preparedStatement = c.prepareStatement("INSERT INTO accommodation VALUES (?,?,?,?,?)");
            preparedStatement.setInt(1, 1);
            preparedStatement.setString(2, "Double Room");
            preparedStatement.setString(3, "Double");
            preparedStatement.setInt(4, 3);
            preparedStatement.setString(5, "All our guestrooms are elegantly furnished with handmade furniture include luxury en-suite facilities with complimentary amenities pack, flat screen LCD TV, tea/coffee making facilities, fan, hairdryer and the finest pure white linen and towels.\n" + "\n");
            preparedStatement.executeUpdate();

            preparedStatement.setInt(1, 2);
            preparedStatement.setString(2, "Single Room");
            preparedStatement.setString(3, "Single");
            preparedStatement.setInt(4, 1);
            preparedStatement.setString(5, "All our guestrooms are elegantly furnished with handmade furniture include luxury en-suite facilities with complimentary amenities pack, flat screen LCD TV, tea/coffee making facilities, fan, hairdryer and the finest pure white linen and towels.\n" + "\n");
            preparedStatement.executeUpdate();

            preparedStatement.setInt(1, 3);
            preparedStatement.setString(2, "Family Room");
            preparedStatement.setString(3, "Double and Single");
            preparedStatement.setInt(4, 4);
            preparedStatement.setString(5, "All our guestrooms are elegantly furnished with handmade furniture include luxury en-suite facilities with complimentary amenities pack, flat screen LCD TV, tea/coffee making facilities, fan, hairdryer and the finest pure white linen and towels.\n" + "\n");
            preparedStatement.executeUpdate();

            preparedStatement = c.prepareStatement("INSERT INTO room_fair VALUES (?,?,?)");
            preparedStatement.setInt(1, 1);
            preparedStatement.setDouble(2, 200d);
            preparedStatement.setString(3, "Winter");
            preparedStatement.executeUpdate();

            preparedStatement.setInt(1, 2);
            preparedStatement.setDouble(2, 400d);
            preparedStatement.setString(3, "Summer");
            preparedStatement.executeUpdate();

            preparedStatement.setInt(1, 3);
            preparedStatement.setDouble(2, 350d);
            preparedStatement.setString(3, "Fall");
            preparedStatement.executeUpdate();

            preparedStatement = c.prepareStatement("INSERT INTO accommodation_room_fair_relation VALUES (?,?,?)");
            preparedStatement.setInt(1, 1);
            preparedStatement.setInt(2, 1);
            preparedStatement.setInt(3, 1);
            preparedStatement.executeUpdate();

            preparedStatement.setInt(1, 2);
            preparedStatement.setInt(2, 2);
            preparedStatement.setInt(3, 3);
            preparedStatement.executeUpdate();

            preparedStatement.setInt(1, 3);
            preparedStatement.setInt(2, 3);
            preparedStatement.setInt(3, 2);
            preparedStatement.executeUpdate();

            preparedStatement = c.prepareStatement("SELECT \n" + "\ta.type, a.bed_type, a.description, rf.value, rf.season\n" + "FROM\n" + "\taccommodation_room_fair_relation arfr\n" + "JOIN accommodation a ON\n" + "\tarfr.accommodation_id = a.id\n" + "JOIN room_fair rf ON\n" + "\tarfr.room_fair_id = rf.id");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                System.out.println("Room with type = " + rs.getString("type") + " and bed type " + rs.getString("bed_type") + " has price " + rs.getString("value") + " for " + rs.getString("season"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");

    }
}
