package week10;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static Function<String, String> findMonth = date -> date.split("\\.", 3)[1];

    public static boolean filterByMonth(String line, String targetMonth) {
        String[] strArray = line.split(",", 3);
        return findMonth.apply(strArray[2]).equals(targetMonth);
    }

    public static List<String> processStream(Stream<String> stream, String targetMonth) {
        return stream
                .filter(line -> Main.filterByMonth(line, targetMonth))
                .sorted()
                .collect(Collectors.toList());
    }

    public static void main(String[] args) throws IOException {

        String inputFileName = "week10.csv";
        List<String> list = new ArrayList<>();
        String targetMonth = "05";
        String outputFileName = "week10_output.csv";

        try (Stream<String> stream = Files.lines(Paths.get((
                ClassLoader.getSystemResource(inputFileName).toURI())))) {

            list = Main.processStream(stream, targetMonth);

        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        FileWriter writer = new FileWriter(outputFileName);
        for (String str : list) {
            writer.write(str + System.lineSeparator());
        }
        writer.close();

    }

}




