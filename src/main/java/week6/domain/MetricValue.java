package week6.domain;

public class MetricValue {

    private int distance;
    private UnitType unitType;

    public MetricValue(int distance, UnitType unitType) {
        this.distance = distance;
        this.unitType = unitType;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(UnitType unitType) {
        this.unitType = unitType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetricValue that = (MetricValue) o;
        return distance == that.distance && unitType == that.unitType;
    }

    @Override
    public String toString() {
        return "Value{" +
                "distance=" + distance +
                ", unit type=" + unitType +
                '}';
    }
}
