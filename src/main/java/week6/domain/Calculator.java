package week6.domain;

public class Calculator {

    Convertor convertor = new Convertor();

    public MetricValue addDistance(MetricValue value1, MetricValue value2) {
        convertor.setUnitTypeToMM(value1);
        convertor.setUnitTypeToMM(value2);
        return new MetricValue(value1.getDistance() + value2.getDistance(), UnitType.MM);
    }

    public MetricValue subtractDistance(MetricValue value1, MetricValue value2) {
        convertor.setUnitTypeToMM(value1);
        convertor.setUnitTypeToMM(value2);
        if (value1.getDistance() > value2.getDistance()) {
            return new MetricValue(value1.getDistance() - value2.getDistance(), UnitType.MM);
        } else {
            throw new RuntimeException("Cannot subtract. Distance 2 > Distance 1.");
        }
    }


}