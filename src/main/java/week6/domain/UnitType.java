package week6.domain;

public enum UnitType {
    MM, CM, DM, M;
}
