package week6.domain;

import java.util.HashMap;
import java.util.Map;

public class Convertor {

    Map<UnitType, Integer> map = new HashMap<>();

    public Convertor() {
        this.map.put(UnitType.MM, 1);
        this.map.put(UnitType.CM, 10);
        this.map.put(UnitType.DM, 100);
        this.map.put(UnitType.M, 1000);
    }

    //TAKES ALL UNIT TYPES AND CONVERTS THEM TO MM
    public void setUnitTypeToMM(MetricValue value) {
        value.setDistance(value.getDistance() * map.get(value.getUnitType()));
        value.setUnitType(UnitType.MM);
    }

    //TAKES RESULT UNIT TYPE AND CONVERTS IT TO USER'S UNIT TYPE CHOICE
    public void getOutputUnitType(MetricValue value, UnitType outputUnitType) {
        if (value.getUnitType().equals(UnitType.MM)) {
            value.setDistance(value.getDistance() / map.get(outputUnitType));
            value.setUnitType(outputUnitType);
        } else {
            throw new RuntimeException("Please insert a MM value");
        }
    }
}