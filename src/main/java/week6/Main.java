package week6;

import week6.domain.UnitType;
import week6.domain.Calculator;
import week6.domain.Convertor;
import week6.domain.MetricValue;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Calculator calculator = new Calculator();
        Convertor convertor = new Convertor();

        MetricValue value1 = new MetricValue(100, UnitType.MM);
        MetricValue value2 = new MetricValue(2, UnitType.M);
        MetricValue value3 = new MetricValue(40, UnitType.CM);
        MetricValue value4 = new MetricValue(10, UnitType.DM);

        // VALUE 1 + VALUE 2
        MetricValue result = calculator.addDistance(value1,value2);
        System.out.println(result);

        // VALUE 1 + VALUE 2 - VALUE 3
        result = calculator.subtractDistance(result, value3);
        System.out.println(result);

        // VALUE 1 + VALUE 2 - VALUE 3 + VALUE 4
        result = calculator.addDistance(result,value4);
        System.out.println(result);

        //USER SELECTS OUTPUT UNIT TYPE
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please select output unit (MM/CM/DM/M)");
        UnitType outputUnitType = UnitType.valueOf(scanner.nextLine());

        //CONVERT RESULT TO SELECTED UNIT TYPE AND PRINT RESULT
        convertor.getOutputUnitType(result, outputUnitType);
        System.out.println(result);

        System.out.println("Lorena");
        System.out.println("Lorena");
    }
}
