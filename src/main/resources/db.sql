CREATE TABLE accommodation (
    id int PRIMARY KEY,
    type varchar(32),
    bed_type varchar(32),
    max_guests int,
    description varchar(512)
);

CREATE TABLE room_fair (
    id int PRIMARY KEY,
    value double precision,
    season varchar(32)
);

CREATE TABLE accommodation_room_fair_relation (
    id int PRIMARY KEY,
    accommodation_id int,
    room_fair_id int,
	FOREIGN KEY (accommodation_id)
		REFERENCES accommodation (id),
	FOREIGN KEY (room_fair_id)
		REFERENCES room_fair (id)
);


SELECT
	a.type, a.bed_type, a.description, rf.value, rf.season
FROM
	accommodation_room_fair_relation arfr
JOIN accommodation a ON
	arfr.accommodation_id = a.id
JOIN room_fair rf ON
	arfr.room_fair_id = rf.id

TRUNCATE TABLE accommodation CASCADE ;
TRUNCATE TABLE room_fair CASCADE ;
TRUNCATE TABLE accommodation_room_fair_relation CASCADE ;






